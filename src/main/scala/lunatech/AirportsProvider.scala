package lunatech

import com.github.tototoshi.csv.CSVReader

import scala.io.Source

object AirportsProvider {
  def fromCsv(
      countriesCsv: Source,
      airportsCsv: Source,
      runwaysCsv: Source
  ): AirportsProvider = {

    val countries = CSVReader
      .open(countriesCsv)
      .all()
      .tail
      .map { r =>
        Country(
          code = r(1).trim.toUpperCase,
          name = r(2).trim,
          keywords = r(5).trim
            .split(',')
            .map(_.trim)
            .filter(_.nonEmpty)
            .toList
        )
      }

    val airports = CSVReader
      .open(airportsCsv)
      .all()
      .tail
      .map { r =>
        Airport(
          ident = r(1).trim.toUpperCase,
          countryCode = r(8).trim.toUpperCase,
          name = r(3).trim
        )
      }

    val runways = CSVReader
      .open(runwaysCsv)
      .all()
      .tail
      .flatMap { r =>
        val ident = r(8).trim.toUpperCase
        val airportIdent = r(2).trim.toUpperCase
        val surface = {
          val v = r(5).trim.toUpperCase
          if (v.isEmpty) {
            None
          } else {
            Some(v)
          }
        }

        if (ident.isEmpty || airportIdent.isEmpty) None
        else
          Some(
            Runway(
              ident = ident,
              airportIdent = airportIdent,
              surface = surface
            ))
      }

    new AirportsProvider(countries, airports, runways)
  }
}

class AirportsProvider(
    val countries: List[Country],
    airports: List[Airport],
    runways: List[Runway]
) {

  private val countryByCode: Map[String, Country] =
    countries.map(c => c.code -> c).toMap

  private val runwaysIdentsByAirport: Map[String, List[String]] = runways
    .groupBy(_.airportIdent)
    .mapValues(rs => rs.map(_.ident).sorted)

  private val airportsByCountry: Map[String, List[Airport]] =
    airports.groupBy(_.countryCode)

  private val airportInfosByCountry: Map[String, List[AirportInfo]] =
    countryByCode.mapValues { country =>
      airportsByCountry(country.code)
        .map { airport =>
          AirportInfo(
            ident = airport.ident,
            name = airport.name,
            runways = runwaysIdentsByAirport.getOrElse(airport.ident, List())
          )
        }
        .sortBy(_.name)
    }

  def airportsByCountry(
      airportsReq: AirportsRequest
  ): Option[AirportsResponse] = {
    val AirportsRequest(countryCode, Page(index, size)) = airportsReq

    airportInfosByCountry.get(countryCode) flatMap {
      list: List[AirportInfo] =>
        val pageCount = Math.ceil(list.length / size.toDouble).toInt
        if (index < pageCount) {
          val entries =
            list.slice(
              from = index * size,
              until = index * size + size
            )

          Some(
            AirportsResponse(
              entries = entries,
              page = AirportsPageInfo(
                countryCode = countryCode,
                index = index,
                pageCount = pageCount,
                entryCount = list.length
              )
            ))
        } else {
          None
        }
    }
  }

  //////////

  private val countryAirportFreqs: List[CountryAirportFreq] =
    airportsByCountry
      .map {
        case (isoCountry, entries) =>
          CountryAirportFreq(
            code = isoCountry,
            name = countryByCode(isoCountry).name,
            count = entries.length
          )
      }
      .toList
      .sortBy(-_.count)

  val top10countries: List[CountryAirportFreq] =
    countryAirportFreqs.take(10)

  val bottomCountries: List[CountryAirportFreq] =
    countryAirportFreqs.filter(_.count < 2).sortBy(_.name)

  /////////

  val mostCommonRunwayIdents: List[RunwayIdentFreq] =
    runways
      .groupBy(runway => runway.ident)
      .map {
        case (ident, entries) =>
          RunwayIdentFreq(ident, entries.length)
      }
      .toList
      .sortBy(-_.count)
      .take(10)

  /////////

  val runwaySurfacesByCountry: List[CountryRunwaySurfaces] = {
    val airportsByIdent: Map[String, Airport] =
      airports.map(airport => airport.ident -> airport).toMap

    val surfacesByCountry: Map[String, List[SurfaceFreq]] =
      runways
        .groupBy { runway =>
          airportsByIdent(runway.airportIdent).countryCode
        }
        .mapValues { runways: List[Runway] =>
          val surfaces: List[String] = runways.flatMap(r => r.surface)

          surfaces
            .groupBy(surface => surface)
            .mapValues(_.length)
            .map {
              case (surface, count) =>
                SurfaceFreq(surface, count)
            }
            .toList
            .sortBy(-_.count)
        }

    countryByCode flatMap {
      case (countryCode, country) =>
        surfacesByCountry.get(countryCode) map { surfaces =>
          CountryRunwaySurfaces(
            countryCode = countryCode,
            countryName = country.name,
            surfaces = surfaces
          )
        }
    }

  }.toList.sortBy(_.countryName)

}
