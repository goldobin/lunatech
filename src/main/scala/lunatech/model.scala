package lunatech

object Defaults {
  val PageMaxSize = 50
}

////

case class Country(
    code: String,
    name: String,
    keywords: List[String] = List()
) {
  require(code.length == 2)
  require(name.nonEmpty)
}

case class Airport(
    ident: String,
    countryCode: String,
    name: String
) {
  require(countryCode.length == 2)
  require(ident.nonEmpty)
  require(name.nonEmpty)
}

case class Runway(
    ident: String,
    airportIdent: String,
    surface: Option[String]
) {
  require(ident.nonEmpty)
  require(airportIdent.nonEmpty)
  require(surface.forall(_.nonEmpty))
}

/////

case class Page(
    index: Int,
    size: Int
) {
  require(index > -1)
  require(size > 0 && size <= Defaults.PageMaxSize)
}

case class AirportsPageInfo(
    countryCode: String,
    index: Int,
    pageCount: Int,
    entryCount: Int
) {
  require(countryCode.length == 2)
  require(index > -1)
  require(pageCount > -1)
  require(entryCount > -1)
}

case class AirportInfo(
    ident: String,
    name: String,
    runways: List[String]
) {
  require(ident.nonEmpty)
  require(name.nonEmpty)
  require(runways.forall(_.nonEmpty))
}

case class AirportsRequest(
    countryCode: String,
    page: Page
) {
  require(countryCode.length == 2)
}

case class AirportsResponse(
    entries: List[AirportInfo],
    page: AirportsPageInfo
)

case class SurfaceFreq(
    name: String,
    count: Int
) {
  require(name.nonEmpty)
  require(count > -1)
}

case class CountryRunwaySurfaces(
    countryCode: String,
    countryName: String,
    surfaces: List[SurfaceFreq]
) {
  require(countryCode.nonEmpty)
  require(countryName.nonEmpty)
}

case class CountryAirportFreq(
    code: String,
    name: String,
    count: Int
) {
  require(code.nonEmpty)
  require(name.nonEmpty)
}

case class RunwayIdentFreq(
    ident: String,
    count: Int
) {
  require(ident.nonEmpty)
  require(count > -1)
}
