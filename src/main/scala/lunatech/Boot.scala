package lunatech

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.{ActorMaterializer, Materializer}
import akka.util.Timeout
import cats.data.Validated
import com.typesafe.scalalogging.StrictLogging
import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import org.json4s.{DefaultFormats, Formats, Serialization, jackson}

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.io.Source
import scala.util.control.NonFatal
import scala.util.{Failure, Success}

object Boot extends App with StrictLogging {

  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val materializer: Materializer = ActorMaterializer()

  val provider = AirportsProvider.fromCsv(
    countriesCsv = Source.fromResource("data/countries.csv"),
    airportsCsv = Source.fromResource("data/airports.csv"),
    runwaysCsv = Source.fromResource("data/runways.csv")
  )

  val route: Route = {
    pathSingleSlash {
      getFromResource("web/index.html")
    } ~
    pathPrefix("static") {
      getFromResourceDirectory("web/static")
    } ~
    pathPrefix("api" / "v1") {

      import Json4sSupport._
      import StatusCodes._

      implicit val serialization: Serialization = jackson.Serialization
      implicit val formats: Formats = DefaultFormats

      implicit val timeout: Timeout = Timeout(5.seconds)

      pathPrefix("countries") {
        path(Segment / "airports") { countryCode =>
          parameters('page.as[Int] ? 0, 'size.as[Int] ? 12) { (index, size) =>
            get {
              rejectEmptyResponse {
                complete {
                  Validation.validateAirportsRequest(countryCode.toUpperCase, index, size) match {
                    case Validated.Invalid(errors) =>
                      BadRequest -> errors.toList.mkString(". ")
                    case Validated.Valid(req) =>
                      OK -> provider.airportsByCountry(req)
                  }
                }
              }
            }
          }
        } ~
        pathEnd {
          complete{
            OK -> provider.countries
          }
        }
      } ~
      pathPrefix("reports") {
        pathPrefix("top-10-countries") {
          complete {
            OK -> provider.top10countries
          }
        } ~
        pathPrefix("bottom-countries") {
          complete {
            OK -> provider.bottomCountries
          }
        } ~
        pathPrefix("runway-surfaces-by-country") {
          complete {
            OK -> provider.runwaySurfacesByCountry
          }
        } ~
        pathPrefix("most-common-runway-idents") {
          complete {
            OK -> provider.mostCommonRunwayIdents
          }
        }
      }
    }
  }

  val listenHost = "localhost"
  val listenPort = 8080

  implicit val executionContext: ExecutionContextExecutor =
    scala.concurrent.ExecutionContext.global

  val bindFuture = Http().bindAndHandle(route, listenHost, listenPort)

  bindFuture onComplete {
    case Success(binding) =>
      logger.info(s"The HTTP server is listening at ${binding.localAddress}")

    case Failure(NonFatal(e)) =>
      logger.error("Can't start server", e)
      actorSystem.terminate().onComplete(_ => System.exit(1))
  }
}
