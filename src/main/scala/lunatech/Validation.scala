package lunatech

import cats.data._
import cats.data.Validated._
import cats.data.NonEmptyList._

import cats.syntax.all._

object Validation {

  private val countryCodePattern = "^[A-Za-z]{2}$".r

  def validateCountryCode(countryCode: String): ValidatedNel[String, String] = {
    countryCodePattern findFirstIn countryCode match {
      case None =>
        invalidNel(
          "The countryCode should be 2 character long and contain only alphabet characters")
      case Some(c) =>
        valid(c)
    }
  }

  def validatePageIndex(index: Int): ValidatedNel[String, Int] = {
    if (index < 0) {
      invalidNel("The page should be non-negative")
    } else {
      valid(index)
    }
  }

  def validatePageSize(size: Int): ValidatedNel[String, Int] = {
    if (size < 1) {
      invalidNel("The size should be non-negative")
    } else if (size >= Defaults.PageMaxSize) {
      invalidNel("The size exceeds the limit")
    } else {
      valid(size)
    }
  }

  def validatePage(index: Int, size: Int): ValidatedNel[String, Page] = {
    (validatePageIndex(index) |@| validatePageSize(size)) map Page
  }

  def validateAirportsRequest(
      countryCode: String,
      index: Int,
      size: Int
  ): ValidatedNel[String, AirportsRequest] = {
    (validateCountryCode(countryCode) |@| validatePage(index, size)) map AirportsRequest
  }
}
