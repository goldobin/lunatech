package lunatech

import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import org.json4s.JsonAST._
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}
import org.scalatest.prop.TableDrivenPropertyChecks

class AirportsResourceSpec
    extends FlatSpec
    with Matchers
    with GivenWhenThen
    with TableDrivenPropertyChecks {

  import HttpMethods._
  import MediaTypes._
  import StatusCodes._
  import Commons._

  "Airports resource" should "check country code" in {
    Given("invalid country codes")

    val invalidCountryCodes = Table(
      "code",
      "01",
      "_1",
      "UKR",
      "U"
    )

    forAll(invalidCountryCodes) { code =>
      val request = HttpRequest(
        method = GET,
        uri = s"${Commons.BaseUrl}/countries/$code/airports"
      )

      When(s"request performed for country code '$code'")
      val response: HttpResponse = awaitResult(Http().singleRequest(request))

      Then("response have status '400 Bad Request'")
      response.status shouldBe BadRequest
    }
  }

  it should "return properly handle unknown country code" in {
    Given("invalid unknown country code")

    val code = "AA"

    val request = HttpRequest(
      method = GET,
      uri = s"${Commons.BaseUrl}/countries/$code/airports"
    )

    When(s"request performed for country code '$code'")
    val response: HttpResponse = awaitResult(Http().singleRequest(request))

    Then("response have status '404 Not Found'")
    response.status shouldBe NotFound
  }

  it should "check page index" in {
    Given("invalid negative page index")
    val index = -1

    val request = HttpRequest(
      method = GET,
      uri = s"${Commons.BaseUrl}/countries/ua/airports?page=$index"
    )

    When(s"request performed")
    val response: HttpResponse = awaitResult(Http().singleRequest(request))

    Then("response have status '400 Bad Request'")
    response.status shouldBe BadRequest
  }

  it should "check page size" in {
    Given("invalid page sizes")
    val invalidPageSizes = Table(
      "size",
      -1,
      0,
      51,
      100
    )

    forAll(invalidPageSizes) { size =>
      val request = HttpRequest(
        method = GET,
        uri = s"${Commons.BaseUrl}/countries/UA/airports?page=0&size=$size"
      )

      When(s"request performed for page size $size")
      val response: HttpResponse = awaitResult(Http().singleRequest(request))

      Then("response have status '400 Bad Request'")
      response.status shouldBe BadRequest
    }
  }

  it should "return airports list with page info" in {
    Given("GET request for airport")

    val request = HttpRequest(
      method = GET,
      uri = s"${Commons.BaseUrl}/countries/gb/airports"
    )

    When("request performed")
    val response: HttpResponse = awaitResult(Http().singleRequest(request))

    Then("response status should be '200 OK'")
    response.status shouldBe OK
    response.entity.contentType.mediaType shouldBe `application/json`
    val json = extractJson(response)

    And("contain page description")
    val pages: List[(String, Int, Int, Int)] = {
      for {
        JObject(page) <- json \ "page"
        JField("countryCode", JString(code)) <- page
        JField("index", JInt(index)) <- page
        JField("pageCount", JInt(pageCount)) <- page
        JField("entryCount", JInt(entryCount)) <- page
      } yield (code, index.toInt, pageCount.toInt, entryCount.toInt)
    }

    pages should have size 1

    val List((code, index, pageCount, entryCount)) = pages

    code shouldBe "GB"
    index shouldBe 0
    pageCount.toInt should be > 0
    entryCount.toInt should be > 0

    And("contain airport array")

    val airports: List[(String, String)] = for {
      JArray(airports) <- json \\ "entries"
      JObject(airport) <- airports
      JField("ident", JString(ident)) <- airport
      JField("name", JString(name)) <- airport
    } yield (ident, name)

    airports should have size 12

    val (ident, name) :: _ = airports
    ident should not be empty
    name should not be empty
  }
}
