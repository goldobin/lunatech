package lunatech

import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}

import org.json4s._

class CountryResourceSpec extends FlatSpec with Matchers with GivenWhenThen {
  import HttpMethods._
  import MediaTypes._
  import StatusCodes._
  import Commons._

  "Countries Resource" should "return correct amount of countries with codes and names" in {

    Given("GET request for countries")

    val request = HttpRequest(
      method = GET,
      uri = s"${Commons.BaseUrl}/countries"
    )

    When("request performed")
    val response: HttpResponse = awaitResult(Http().singleRequest(request))

    Then("response status should be '200 OK'")
    response.status shouldBe OK
    response.entity.contentType.mediaType shouldBe `application/json`

    And("contain an array of countries")
    val json = extractJson(response)

    val codes: List[String] = for {
      JArray(countries) <- json
      JObject(country) <- countries
      JField("code", JString(code)) <- country
    } yield code

    codes.distinct should have size 247

    And("each country has 2 character code")
    codes.forall(_.length == 2) shouldBe true

    val names: List[String] = for {
      JArray(countries) <- json
      JObject(country) <- countries
      JField("name", JString(name)) <- country
    } yield name

    And("each country has non empty name")
    names.forall(_.trim.nonEmpty) shouldBe true
  }

}
