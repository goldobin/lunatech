package lunatech

import java.nio.charset.StandardCharsets

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpEntity, HttpResponse}
import akka.stream.ActorMaterializer
import akka.util.ByteString

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

object Commons {

  val BaseUrl = "http://localhost:8080/api/v1"

  implicit val System: ActorSystem = ActorSystem()
  implicit val Materializer: ActorMaterializer = ActorMaterializer()

  private val defaultTimeout = 5.seconds

  def parseJson(raw: ByteString) = {
    import org.json4s._
    import org.json4s.jackson.JsonMethods._

    parse(raw.decodeString(StandardCharsets.UTF_8))
  }

  def extractJson(response: HttpResponse) = {
    parseJson(awaitHttpEntity(response.entity).data)
  }

  def awaitHttpEntity(entity: HttpEntity): HttpEntity.Strict = {
    awaitResult(entity.toStrict(1.second))
  }

  def awaitResult[T](f: Future[T]): T = {
    Await.result(f, defaultTimeout)
  }
}
